/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {createStackNavigator, createAppContainer} from 'react-navigation';

import login from './components/Login'
import forgotPassword from './components/ForgotPassword'
import dashboard from './components/Dashboard'
import refrigeracao from './components/Refrigeracao'
import construcaoCivil from './components/ConstrucaoCivil'
import detalheOrcamento from './components/DetalheOrcamento'
import verOrcamentos from'./components/dados/VerOrcamentos'
import verOrcamentoEspecifico from './components/dados/VerOrcamentoEspecifico'
import updateOrcamento from './components/dados/UpdateOrcamento'
import deleteOrcamento from './components/dados/DeleteOrcamento'

const AppNavigator = createStackNavigator(
  {
    TelaLogin: {screen: login, navigationOptions: {header: null}},
    TelaEsqSenha:  {screen: forgotPassword, navigationOptions: {header: null}},
    TelaDashboard: {screen: dashboard, navigationOptions: {header: null}},
    TelaRefrigeracao: {screen: refrigeracao, navigationOptions: {header: null}},
    TelaConstrucaoCivil: {screen: construcaoCivil, navigationOptions: {header: null}},
    TelaDetalhesOrcamento: {screen: detalheOrcamento, navigationOptions: {header: null}},
    TelaVerOrcamento: {screen: verOrcamentos, navigationOptions: {header: null}},
    TelaVerOrcamentoEspecifico: {screen: verOrcamentoEspecifico, navigationOptions: {header: null}},
    TelaUpdateOrcamento: {screen: updateOrcamento, navigationOptions: {header: null}},
    TelaDeleteOrcamento: {screen: deleteOrcamento, navigationOptions: {header: null}}
  },
  {
    initialRouteName: 'TelaDashboard'
  }
);

const AppContainer = createAppContainer(AppNavigator);

export default class App extends Component{
  render(){
    return(
      <AppContainer />
    );
  }
}

