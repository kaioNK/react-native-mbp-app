import React, {Component} from 'react';
import {StyleSheet, Text, View, ImageBackground, Image, Dimensions, TextInput, Alert} from 'react-native';

/*Usando uma biblioteca para os Botões:
https://github.com/APSL/react-native-button
*/
import Button from 'apsl-react-native-button'

//Imagem background
import bgImage from '../images/azul.jpeg' 
//Imagem logo
import logo from '../images/React.js_logo-512.png'

const { width: WIDTH } = Dimensions.get('window')

export default class ForgotPassword extends Component {

  buttonClickded = () => {
    Alert.alert(
      "Atenção:",
      "Link enviado com sucesso!",
      [
        { text: "OK", onPress: () => console.log("OK Pressed")}
      ],
      { cancelable: false }
    );
  };


  render(){
    return(
      <ImageBackground source={bgImage} style={styles.backgroundContainer}>
        <View style={styles.logoContainer}>
          <Image source={logo} style={styles.logo} /> 
          <Text style={styles.logoText}>EMPRESA X</Text>
        </View>

        <View style={styles.inputContainer}>
          <Text style={styles.labelInfo}>
            Informe seu email para que possamos enviar um link para a recuperação de senha:
          </Text>
        </View>

        <View style={styles.inputContainer}>
          
          {/*Input Email*/}
          <TextInput 
            style={styles.input}
            placeholder={'Email'}
            placeholderTextColor={'rgba(255,255,255,0.7)'}
            underlineColorAndroid='transparent'
            autoCorrect={false}
            returnKeyType="done"
            keyboardType="email-address"
           
          />
        </View>

        <View style={styles.buttonContainer}>
          {/*
            <Button style={styles.button}
            title="Enviar"
            color="#14873b"
            onPress={this.buttonClickded}
            />
          */}
          <Button 
            style={{marginTop: 15}}  
            onPress={this.buttonClickded} >
              Enviar
          </Button>
          
        </View>

      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  backgroundContainer: {
    flex: 1,
    width: null,
    height: null,
    justifyContent: 'center',
    alignItems: 'center',
  },
  logo: {
    width: 120,
    height: 120,
    opacity: 0.2
  },
  logoContainer: {
    alignItems: 'center',
    marginBottom: 50
  },
  logoText: {
    color: 'white',
    fontSize: 25,
    fontWeight: '500',
    marginTop: 10,
    opacity: 0.5
  },
  inputContainer: {
    marginTop: 5
  },
  input: {
    width: WIDTH -55,
    height: 45,
    marginTop: 5,
    borderRadius: 25,
    fontSize: 16,
    paddingLeft: 25,
    backgroundColor: 'rgba(0, 0, 0, 0.35)',
    color: 'rgba(255, 255, 255, 0.7)'
  },
  labelInfo: {
    fontSize: 18,
    fontWeight: 'bold',
    padding: 15,
    width: WIDTH -12,
    textAlign: 'auto',
    letterSpacing: 2.5,
    color: 'black',
    lineHeight: 20
  },
  buttonContainer: {
    marginTop: 20,
    width: 125,
  },

});