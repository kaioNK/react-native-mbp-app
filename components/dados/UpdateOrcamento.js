import React from 'react'
import {View, Text, Alert, StyleSheet, ScrollView, TextInput} from 'react-native'
import { Divider, Button, Input } from 'react-native-elements';
import RNPickerSelect from 'react-native-picker-select';
import {AutoGrowingTextInput} from 'react-native-autogrow-textinput'
import { TextInputMask } from 'react-native-masked-text'
import {BD, TABLE, PK} from '../bd_config/banco'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'


import {openDatabase} from 'react-native-sqlite-storage'

const db = openDatabase({name: BD})

const valorNulo = 'R$0,00'

const servicoConstrucao = [
  {label: 'Chapas', value: 'Chapas'},
  {label: 'Acessórios', value:'Acessorios'},
  {label: 'Alvenaria', value: 'Alvenaria'}
]

const servicoRefrigeracao = [
  {label: 'Limpeza', value: 'Limpeza'},
  {label: 'Manutenção', value: 'Manutenção'}
]

const servicoNull = [
  {label: '', value: null}
]

export default class UpdateOrcamento extends React.Component{

  state = {
    cliente: '',
    servico: '',
    complemento: '',
    tipo: '',
    valor: ''
  }

  //DEBUG para ver se a busca está OK
  search = (id_orcamento) => {
    //console.log('ID: ', id_orcamento)
    const id2 = id_orcamento
    //console.log('ID: ', id2)
    db.transaction(tx => {
      tx.executeSql(
        'SELECT * FROM ' + TABLE + ' WHERE ' + PK + ' = ?', [id2], (tx, results) => {
          const len = results.rows.length
          console.log('Len: ', len)
          if(len > 0){
            this.setState({
              cliente: results.rows.item(0).cliente,
              servico: results.rows.item(0).servico,
              complemento: results.rows.item(0).complemento,
              tipo: results.rows.item(0).tipo,
              valor: results.rows.item(0).valor
            })
          }else{
            Alert.alert('ATENÇÃO', 'Orçamento não encontrado!')
            this.setState({
              cliente: '',
              servico: '',
              complemento: '',
              tipo: '',
              valor: '',
            })
          }
        }
      )
    })
  }

  //se retornar 0: o usuário não digitou nada (tá R$0,00)
  //se retornar 1: o usuário digitou mas depois apagou (ficou R$0,00)
  verificaValorNulo = (val) => {
    return valorNulo.localeCompare(val)
  }

  update = (id) => {
    console.log('=> update')
    let that = this
    const {cliente, servico, complemento, tipo, valor} = this.state
    console.log('cliente: ', cliente)
    if(cliente){
      if(tipo){
        if(servico){
          if(this.verificaValorNulo(valor) == -1){
            db.transaction(tx => {
              tx.executeSql(
                'UPDATE ' + TABLE + ' set cliente=?, tipo=?, servico=?, complemento=?, valor=? where id = ?', [cliente, tipo, servico, complemento, valor, id],
                (tx, results) => {
                  if(results.rowsAffected > 0){
                    Alert.alert('Sucesso', 'Dados foram atualizados com sucesso!',
                      [
                        {text: 'Ok', onPress: () => that.props.navigation.navigate('TelaDashboard')},
                      ],
                      {cancelable: false}
                    )
                  }else{
                    Alert.alert('ATENÇÃO', 'Falha na atualização dos dados.')
                  }
                }
              )
            })
          }else{
            Alert.alert('ATENÇÃO:', 'Por favor preencha o valor do serviço.')
          }
        }else{
          Alert.alert('ATENÇÃO:','Por favor selecione o tipo de serviço.')
        }
      }else{
        Alert.alert('ATENÇÃO:','Por favor selecione o tipo de serviço.')
      }
    }else{
      Alert.alert('ATENÇÃO:','Por favor informe o nome do cliente.')
    }
  }

  //DEBUG 2
  print = () => {
    console.log('state tipo: ', this.state.tipo)
    console.log('state tipo é null? ', this.state.tipo === null)
    console.log('state tipo: ', this.state.tipo === '')
  }

  render(){
    /*Pegando os valores originais para exibir*/
    const current_id = this.props.navigation.state.params.current_id
    const current_cliente = this.props.navigation.state.params.current_cliente
    const current_servico = this.props.navigation.state.params.current_servico
    const current_tipo = this.props.navigation.state.params.current_tipo
    const current_complemento = this.props.navigation.state.params.current_complemento
    const current_valor = this.props.navigation.state.params.current_valor
    
    //const {cliente, servico, complemento, tipo, valor} = this.state

    const {goBack} = this.props.navigation

    return(
      <View style={{flex: 1}}>
        {/*Titulo*/}
        <Text style={styles.title}>
          ATUALIZAÇÃO
        </Text> 

        <ScrollView>
          <Text style={styles.subtitle}>DADOS ATUAIS</Text>
          <View style={styles.container}>
            <Text style={styles.text}>ID: {current_id}</Text>
            <Divider />
            <Text style={styles.text}>CLIENTE: {current_cliente}</Text>
            <Divider />
            <Text style={styles.text}>TIPO: {current_tipo}</Text>
            <Divider />
            <Text style={styles.text}>SERVIÇO: {current_servico}</Text>
            <Divider />
            <Text style={styles.text}>COMPLEMENTO: {current_complemento}</Text>
            <Divider />
            <Text style={styles.text}>VALOR: {current_valor}</Text>
          </View>

          {/*Edição dos Dados*/}
          <Divider style={{height: 2, marginTop: 10}} />
          <Text style={styles.subtitle}>EDIÇÃO DOS DADOS</Text>

          <View style={{marginTop:10}}>
            <Input 
              label='CLIENTE'
              //placeholder={current_cliente}
              onChangeText={input => this.setState({cliente: input})}
              value={this.state.cliente}
              rightIcon={
                <Icon 
                  name='account'
                  size={28}
                />
              }
              
            />
          </View>
          
          <View style={{marginTop:10}}>
            <Text style={{marginLeft: 10, fontSize: 16, fontWeight:'bold', color: '#86939e'}}>TIPO</Text>
            <RNPickerSelect
              placeholder={{label: 'Selecione o tipo...', value: null, color:'#9EA0A4'}}
              onValueChange={(value) => this.setState({tipo: value})}
              items={[
                { label: 'CONSTRUÇÃO CIVIL', value: 'Construcao' },
                { label: 'REFRIGERAÇÃO', value: 'Refrigeracao' },
              ]}
              style={{
                iconContainer: {
                  right: 18,
                },
              }}
            />
          </View>

          <View style={{marginTop:10}}>
            <Text style={{marginLeft: 10, fontSize: 16, fontWeight:'bold', color: '#86939e'}}>SERVIÇO</Text>
            <RNPickerSelect
              placeholder={{label: 'Selecione o serviço...', value: null, color:'#9EA0A4'}}
              items={this.state.tipo === '' || null ? servicoNull : this.state.tipo === 'Construcao' ? servicoConstrucao : servicoRefrigeracao}
              onValueChange={(value) => this.setState({servico: value})}
              style={{
                iconContainer: {
                  right: 18,
                },
              }}
            />
          </View>
          
          <View style={{marginTop:10}}>
            {/*Text Area*/}
            <Text style={{marginLeft: 10, fontSize: 16, fontWeight:'bold', color: '#86939e'}}>
              COMPLEMENTO
            </Text>

            <AutoGrowingTextInput style={{ marginLeft: 20, marginRight: 20}}>
              <TextInput
                style={styles.inputTextArea}
                underlineColorAndroid="transparent"
                placeholder="Digite informações adicionais aqui"
                placeholderTextColor="grey"
                multiline={true}
                returnKeyType='done'
                onChangeText={input => this.setState({complemento: input})}
              />
              </AutoGrowingTextInput>
          </View>
          
          <View style={{marginTop:10}}>
            {/*Valor*/}
            <Text style={{marginLeft: 10, fontSize: 16, fontWeight:'bold', color: '#86939e'}}>
              VALOR (R$)
            </Text>

            {/*Usando biblioteca externa para os valores. ATENÇÃO: o valor será uma String*/}
            <TextInputMask
              style={styles.input}
              type={'money'}
              options={{
                precision: 2,
                separator: ',',
                delimiter: '.',
                unit: 'R$',
                suffixUnit: ''
              }}
              value={this.state.valor}
              onChangeText={text => {
                this.setState({
                  valor: text
                })
              }}
            />
            
          </View>
          
          {/*BOTÃO ATUALIZAR*/}
          <View style={{flexDirection: 'row', justifyContent: 'center'}}>
            <Button
              buttonStyle={styles.btnEdit}
              titleStyle={{fontSize: 18, fontWeight: 'bold'}}
              title='ATUALIZAR'
              icon={
                <Icon 
                  name='sync'
                  size={18}
                  color='white'
                  style={{marginLeft: 25}}
                />
              }
              iconRight
              onPress={() => this.update(current_id)}
            />
          </View>

           {/*BOTÃO CANCELAR*/}
           <View style={{flexDirection: 'row', justifyContent: 'center'}}>
            <Button
              buttonStyle={styles.btnBack}
              titleStyle={{fontSize: 18, fontWeight: 'bold'}}
              title='CANCELAR'
              icon={
                <Icon 
                  name='reply'
                  size={25}
                  color='white'
                  style={{marginLeft: 20}}
                />
              }
              iconRight
              onPress={() => goBack()}
            />
          </View>
          
        </ScrollView>
        
        {/*DEBUG*/}
        {/*<Button title="Clica" onPress={() => this.search(id)}></Button>*/}
        {/*<Button title="Clica" onPress={() => this.print()}></Button>*/}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  title:{
    fontSize: 25, 
    fontWeight:'bold',      
    textAlign: 'center',
    textAlignVertical: 'center',
    backgroundColor: '#FF9932',
    color: '#fff',
    height: 50
  },
  container:{
    marginRight: 25,
    marginLeft: 25,
    marginTop: 10,
  },
  subtitle:{
    fontSize: 20, 
    fontWeight:'bold',      
    textAlign: 'center',
    marginTop: 10
  },
  text:{
    fontSize: 18,
    padding: 5,
  },
  selectTextArea: {
    borderColor:'grey', 
    borderWidth: 1, 
    marginTop: 10,
    marginLeft: 20,
    marginRight: 20,
    fontSize: 18
  },
  input: {
    marginTop: 2,
    marginLeft: 10,
    width: 360,
    borderBottomColor: 'gray', 
    borderBottomWidth: 1,
    fontSize: 18
  },
  btnEdit:{
    backgroundColor:"#196be8",
    marginTop: 40,
    width: 250
  },
  btnBack: {
    backgroundColor:"#FF9932",
    marginTop: 20,
    marginBottom: 40,
    width: 250,
  },
})