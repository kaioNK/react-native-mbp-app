/**
 * TELA QUE NÃO ESTOU MAIS USANDO!!!
 * => Como era antes: na tela 'VerOrcamentoEspecifico' clicando no botão 'Deletar' iria para esta tela, onde fazia uma nova consulta (só para ter certeza que pegou os dados corretos) para aí sim poder deletar.
 * => Como é agora: a deleção é feita na própria tela 'VerOrcamentoEspecifico' ao clicar no botão 'Deletar'.
 */

import React from 'react'
import {Text, View, Alert, StyleSheet} from 'react-native'
import { Divider, Button, Header } from 'react-native-elements';
import Icon from 'react-native-vector-icons/Octicons'
import {openDatabase} from 'react-native-sqlite-storage'
import {BD, TABLE, PK} from '../bd_config/banco'

const db = openDatabase({name: BD})

export default class DeleteOrcamento extends React.Component{
  
  delete = (id) => {
    let that = this
    db.transaction(tx => {
      tx.executeSql(
        'DELETE FROM ' + TABLE + ' where ' + PK +' = ?', [id],
        (tx, results) => {
          if(results.rowsAffected > 0){
            Alert.alert('SUCESSO: ', 'Item deletado!',
              [{
                text: 'Ok',
                onPress: () => that.props.navigation.navigate('TelaDashboard')
              }], {cancelable: false}
            )
          }
          else{
            Alert.alert('ATENÇÃO: ', 'Erro: item não encontrado!')
          }
        }
      )
    }) 
  }

  confirm = (id) => {
    Alert.alert(
      'ATENÇÃO:',
      'Você tem certeza que deseja DELETAR este item?',
      [
        {text: 'SIM', onPress: () => this.delete(id)},
        {text: 'NÃO'},
      ], {cancelable: false}
    )
  }

  render(){
    const current_id = this.props.navigation.state.params.current_id
    const current_cliente = this.props.navigation.state.params.current_cliente
    const current_tipo = this.props.navigation.state.params.current_tipo
    const current_servico = this.props.navigation.state.params.current_servico
    const current_valor = this.props.navigation.state.params.current_valor
    const current_complemento = this.props.navigation.state.params.current_complemento

    const {goBack} = this.props.navigation

    return(
      <View style={{flex:1}}>
        <Header 
          leftComponent={
            <Icon
              name='arrow-left'
              size={30}
              color='white'
              onPress={() => goBack()}
              style={{marginLeft: 15}}
            />
          }
          centerComponent={{
            text: `ORÇAMENTO Nº: ${current_id}`,
            style: {color: '#fff', fontSize: 18,
            fontWeight: 'bold'}
          }}
          backgroundColor='#FF9932'
        />
        <View style={styles.container}>
          <Text style={styles.text}>ID: {current_id}</Text>
          <Divider />
          <Text style={styles.text}>CLIENTE: {current_cliente}</Text>
          <Divider />
          <Text style={styles.text}>TIPO: {current_tipo}</Text>
          <Divider />
          <Text style={styles.text}>SERVIÇO: {current_servico}</Text>
          <Divider />
          <Text style={styles.text}>VALOR: {current_valor}</Text>
          <Divider />

        </View>

        <View style={{flexDirection: 'row', justifyContent: 'center'}}>
          <Button
            buttonStyle={styles.btnDelete}
            titleStyle={{fontSize: 18, fontWeight: 'bold'}}
            title='DELETAR'
            icon={
              <Icon 
                name='trashcan'
                size={18}
                color='white'
                style={{marginLeft: 25}}
              />
            }
            iconRight
            onPress={() => this.confirm(current_id)}
          />
        </View>
      
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container:{
    marginRight: 25,
    marginLeft: 25,
    marginTop: 20
  },
  text:{
    fontSize: 18,
    padding: 5
  },
  btnDelete:{
    backgroundColor: '#ff3d33',
    marginTop: 20,
    width: 250
  }
})