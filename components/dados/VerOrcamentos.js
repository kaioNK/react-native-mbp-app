import React from 'react'
import {FlatList, Text, View, StyleSheet, ScrollView, TouchableOpacity } from 'react-native'
import {Header} from 'react-native-elements'
import Icon from 'react-native-vector-icons/Octicons'
import {openDatabase} from 'react-native-sqlite-storage'
import {BD, TABLE} from '../bd_config/banco'

const db = openDatabase({name: BD})

export default class VerOrcamentos extends React.Component{

  constructor(props){
    super(props)
    this.state = {
      FlatListItems: []
    }

    db.transaction(tx => {
      tx.executeSql('SELECT * FROM ' + TABLE, [], (tx, results) => {
        let temp = []
        for(let i = 0; i < results.rows.length; i++){
          temp.push(results.rows.item(i))
        }
        this.setState({
          FlatListItems: temp
        })
      })
    })
  }

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          backgroundColor: "#CED0CE",
        }}
      />
    );
  };

  tipoServico = (dado) => {
    if(dado === 'Construcao'){
      return 'Construção'
    }
    if(dado === 'Refrigeracao'){
      return 'Refrigeração'
    }else{
      return 'Nenhum serviço selecionado'
    }
  }
  
  render(){
    return(
      <View style={{flex: 1}}>

        {/*Titulo*/}
        {/*<View style={{backgroundColor: '#FF9932'}}>
          <Text style={styles.title}>
            TODOS OS ORÇAMENTOS
          </Text>
    </View>*/}
        
        <Header 
          leftComponent={
            <Icon 
            name='arrow-left'
            size={30}
            color='white'
            onPress={() => this.props.navigation.navigate('TelaDashboard')}
            style={{marginLeft: 15}}
            />
          }
          centerComponent={{
            text: 'TODOS OS ORÇAMENTOS', 
            style: {color: '#fff', fontSize: 18, fontWeight:'bold'}
          }}
          rightComponent={
            <Icon 
              name='search'
              size={20}
              color='white'
              //onPress={() => this.props.navigation.navigate('TelaDashboard')}
              style={{marginRight: 15}}
            />
          }
          backgroundColor='#FF9932'
        />
        
        <FlatList 
          data={this.state.FlatListItems}
          style={{marginLeft: 20, marginRight: 20, marginTop: 20}}
          ItemSeparatorComponent={this.renderSeparator}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({item}) => (
            <TouchableOpacity  onPress={() => this.props.navigation.navigate('TelaVerOrcamentoEspecifico', {id: item.id, cliente: item.cliente, tipo: this.tipoServico(item.tipo), servico: item.servico, valor: item.valor, complemento: item.complemento})}>
              <View key={item.id} style={{padding: 10}}>
                <Text style={{fontSize: 16}}>ID: {item.id}</Text>
                <Text  style={{fontSize: 16}}>CLIENTE: {item.cliente}</Text>
                <Text style={{fontSize: 16}}>TIPO: {this.tipoServico(item.tipo)}</Text>
                <Text style={{fontSize: 16}}>SERVIÇO: {item.servico}</Text>
                <Text style={{fontSize: 16}}>VALOR: {item.valor}</Text>
                <Text>COMPLEMENTO: {item.complemento}</Text>
                <Text>{item.complemento !== '' ? `COMPLEMENTO: ${item.complemento}` : null}</Text>
              </View>
            </TouchableOpacity >  
          )}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  title:{
    fontSize: 25, 
    fontWeight:'bold',      
    textAlign: 'center',
    textAlignVertical: 'center',
    //backgroundColor: '#FF9932',
    color: '#fff',
    height: 50
  }
})