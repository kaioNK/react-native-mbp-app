import React from 'react'
import {View, Text, StyleSheet, Alert} from 'react-native'
import { Divider, Button, Header } from 'react-native-elements';
import Icon from 'react-native-vector-icons/Octicons';

/*Usando uma biblioteca para os Botões:
https://github.com/APSL/react-native-button
*/
//import Button from 'apsl-react-native-button'

import {openDatabase} from 'react-native-sqlite-storage'

import {BD, TABLE, PK} from '../bd_config/banco'

const db = openDatabase({name: BD})

export default class VerOrcamentoEspecifico extends React.Component{

  delete = (id) => {
    let that = this
    db.transaction(tx => {
      tx.executeSql(
        'DELETE FROM ' + TABLE + ' where ' + PK +' = ?', [id],
        (tx, results) => {
          if(results.rowsAffected > 0){
            Alert.alert('ATENÇÃO: ', 'Orçamento deletado com sucesso!',
              [{
                text: 'Ok',
                onPress: () => that.props.navigation.navigate('TelaDashboard')
              }], {cancelable: false}
            )
          }
          else{
            Alert.alert('ATENÇÃO: ', 'Erro: item não encontrado!')
          }
        }
      )
    }) 
  }

  confirm = (id) => {
    Alert.alert(
      'ATENÇÃO:',
      'Você tem certeza que deseja DELETAR este item?',
      [
        {text: 'SIM', onPress: () => this.delete(id)},
        {text: 'NÃO'},
      ], {cancelable: false}
    )
  }

  render(){
    const id =  this.props.navigation.state.params.id
    const cliente = this.props.navigation.state.params.cliente 
    const tipo = this.props.navigation.state.params.tipo
    const servico = this.props.navigation.state.params.servico
    const valor = this.props.navigation.state.params.valor
    const complemento = this.props.navigation.state.params.complemento

    const {goBack} = this.props.navigation
    return(
      <View style={{flex: 1}}>
        {/*Titulo*/}
        {/*<Text style={styles.title}>
          ORÇAMENTO Nº: {id}
        </Text>*/}

        <Header 
          leftComponent={
            <Icon 
              name='arrow-left'
              size={30}
              color='white'
              onPress={() => goBack()}
              style={{marginLeft: 15}}
            />
          }
          centerComponent={{
            text: `ORÇAMENTO Nº: ${id}`,
            style: {color: '#fff', fontSize: 18, fontWeight: 'bold'}
          }}
          backgroundColor='#FF9932'
        />

        <View style={styles.container}>
          <Text style={styles.text}>ID: {id}</Text>
          <Divider />
          <Text style={styles.text}>CLIENTE: {cliente}</Text>
          <Divider />
          <Text style={styles.text}>TIPO: {tipo}</Text>
          <Divider />
          <Text style={styles.text}>SERVIÇO: {servico}</Text>
          <Divider />
          <Text style={styles.text}>VALOR: {valor}</Text>
          <Divider />
        </View>

        <View style={{flexDirection: "row", justifyContent: 'center'}}>
          <Button 
            buttonStyle={styles.btnEdit}
            titleStyle={{fontSize: 18, fontWeight: 'bold'}}
            title="EDITAR"
            icon={
              <Icon 
                name='pencil'
                size={18}
                color='white'
                style={{marginLeft: 25}}
              />
            }
            iconRight  
            onPress={() => this.props.navigation.navigate('TelaUpdateOrcamento', {current_id: id, current_cliente: cliente, current_servico: servico, current_complemento: complemento, current_tipo: tipo, current_valor: valor})}
          />
        </View>
        
        <Divider style={{marginTop: 30}} />

        <View style={{flexDirection: "row", justifyContent: 'center'}}>
          <Button 
            buttonStyle={styles.btnDelete}
            titleStyle={{fontSize: 18, fontWeight: 'bold'}}
            title="DELETAR"
            icon={
              <Icon 
                name='trashcan'
                size={18}
                color='white'
                style={{marginLeft: 25}}
              />
            }
            iconRight  
            onPress={() => this.confirm(id)}
          />
        </View>

      </View>
    )
  }
}

const styles = StyleSheet.create({
  title:{
    fontSize: 25, 
    fontWeight:'bold',      
    textAlign: 'center',
    textAlignVertical: 'center',
    backgroundColor: '#FF9932',
    color: '#fff',
    height: 50
  },
  container:{
    marginRight: 25,
    marginLeft: 25,
    marginTop: 20,
  },
  text:{
    fontSize: 18,
    padding: 5,
  },
  btnEdit:{
    backgroundColor:"#196be8",
    marginTop: 20,
    width: 250
  }, 
  btnDelete:{
    backgroundColor:"#ff3d33",
    marginTop: 20,
    width: 250,
  }, 
  btnVoltar:{
    backgroundColor:"#FF9932",
    marginTop: 20,
    width: 250
  }
  
})