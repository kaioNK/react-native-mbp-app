import React, {Component} from 'react';
import {StyleSheet, Text, View, TextInput, ScrollView} from 'react-native';
import { Divider, Button } from 'react-native-elements';
import {openDatabase} from 'react-native-sqlite-storage'

import {BD, TABLE} from './bd_config/banco'

const db = openDatabase({name: BD})

/*Usando uma biblioteca para os Botões:
https://github.com/APSL/react-native-button
*/
//import Button from 'apsl-react-native-button'

export default class Dashboard extends Component{
  constructor(props){
    super(props)
    db.transaction(function(tx){
      tx.executeSql(
        "select name from sqlite_master where type='table' and name = '"+TABLE+"'", [], function(tx, res){
          console.log('item: ', res.rows.length)
          if(res.rows.length === 0){
            tx.executeSql('drop table if exists ' + TABLE, [])
            tx.executeSql('create table if not exists ' + TABLE + ' (id integer primary key autoincrement, cliente varchar(255), servico varchar(100), complemento varchar(255), valor decimal(10,2), tipo varchar(30) )')
          }
        }
      )
    })
  }
  render(){
    return(
      <View style={{flex: 1}}>
         {/*Titulo*/}
         <Text style={styles.title}>
          TIPOS DE ORÇAMENTO
        </Text>

        <ScrollView>
          {/*Botões*/}
          <View style={{marginTop: 30}}>
            <View style={styles.container}>
              <Button 
                title="REFRIGERAÇÃO"
                buttonStyle={styles.button}
                titleStyle={styles.textBtn}
                onPress={() => this.props.navigation.navigate('TelaRefrigeracao')} />
            </View>

            <View style={styles.container}>
              <Button 
                title="CONSTRUÇÃO CIVIL"
                buttonStyle={styles.button} 
                titleStyle={styles.textBtn} 
                onPress={() => this.props.navigation.navigate('TelaConstrucaoCivil')} />
            </View>

            <Divider 
              style={{backgroundColor: '#acacac', padding: 0.5, marginTop: 10}}
            />

            <View style={styles.container}>
              <Button 
                title="VER ORÇAMENTOS"
                buttonStyle={styles.button2} 
                titleStyle={styles.textBtn} onPress={() => this.props.navigation.navigate('TelaVerOrcamento')} />
            </View>

          </View>
      
        </ScrollView>

      </View>
    )
  }
}

const styles = StyleSheet.create({
  title: {
    fontSize: 25, 
    fontWeight:'bold',      
    textAlign: 'center',
    textAlignVertical: 'center',
    backgroundColor: '#FF9932',
    color: '#fff',
    height: 50
  },
  container: {
    flexDirection: "row", 
    justifyContent: "center"
  },
  textBtn:{
    fontSize: 18,
    fontWeight: 'bold',
    color: '#fff'
  },
  button: {
    backgroundColor:"#FF9932",
    marginTop: 20,
    width: 250,
  },
  button2: {
    backgroundColor:"#f17743",
    marginTop: 15,
    width: 250,
  },
})