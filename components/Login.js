import React, {Component} from 'react';
import {StyleSheet, Text, View, ImageBackground, Image, Dimensions, TextInput, TouchableOpacity, Alert} from 'react-native';

/*Usando uma biblioteca para os Botões:
https://github.com/APSL/react-native-button
*/
import Button from 'apsl-react-native-button'

//Imagem background
import bgImage from '../images/azul.jpeg' 

//Imagem logo
import logo from '../images/React.js_logo-512.png'


const { width: WIDTH } = Dimensions.get('window');

export default class Login extends Component {
  state = {
    email: '',
    password: ''
  }

  sendInput = e => {
    e.preventDefault()
  
    const url = 'http://142.93.120.226/mbp_api/web/index.php/auth/login'
    
    fetch(url, {
      method: 'post',
      headers: {
        'Accept' : 'application/json, text/plain, */*',
        'Content-Type' : 'application/json'
      },
      body: JSON.stringify({
        LoginForm: {
          username: this.state.email,
          password: this.state.password
        }
      }),
    }).then(response => {
      if(response.status === 401){
        Alert.alert('Atenção:', 'Credenciais inválidas!')
        //console.log(response.status)
      }else{
        this.props.navigation.navigate('TelaDashboard')
        //console.log(response.status)
      }
    })
    //console.log(`Email: ${this.state.email}\nPassword: ${this.state.password}`)
  }

  render() {
    return (
      <View  style={styles.backgroundContainer}> 
        <View style={styles.logoContainer}>
          <Image source={logo} style={styles.logo} /> 
          <Text style={styles.logoText}>GRUPO MBP</Text>
        </View>

        <View style={styles.inputContainer}>
          {/*Input Usuário*/}
          <TextInput 
            keyboardType="email-address"
            autoCapitalize='none'
            style={styles.input}
            placeholder='Email'
            placeholderTextColor={'rgba(255,255,255,0.7)'}
            underlineColorAndroid='transparent'
            autoCorrect={false}
            returnKeyType="next"
            onChangeText={email => this.setState({email})}
          />
        </View>

        {/*Input Senha*/}
        <View style={styles.inputContainer}>
          <TextInput 
            style={styles.input}
            placeholder='Senha'
            secureTextEntry={true}
            placeholderTextColor={'rgba(255,255,255,0.7)'}
            underlineColorAndroid='transparent'
            autoCorrect={false}
            returnKeyType="done"
            onChangeText={password => this.setState({password})}
          />
        </View>
        
        <View style={styles.buttonLogin}>
          <Button 
            style={{width: 140, backgroundColor: '#fff'}} 
            textStyle={{fontWeight:'bold'}} 
            onPress={this.sendInput}>
            Login
          </Button>
        </View>
        
        <TouchableOpacity onPress={() => this.props.navigation.navigate('TelaEsqSenha')}>
          <Text style={styles.textEsqSenha}>Esqueci minha senha</Text>
        </TouchableOpacity>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  backgroundContainer: {
    flex: 1,
    width: null,
    height: null,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FF9932'
  },
  logo: {
    width: 120,
    height: 120,
    opacity: 0.2
  },
  logoContainer: {
    alignItems: 'center',
    marginBottom: 50
  },
  logoText: {
    color: '#000',
    fontSize: 25,
    fontWeight: '500',
    marginTop: 10,
    opacity: 0.5
  },
  input: {
    width: WIDTH -55,
    height: 45,
    marginTop: 5,
    borderRadius: 25,
    fontSize: 16,
    paddingLeft: 25,
    backgroundColor: 'rgba(0, 0, 0, 0.15)',
    color: 'rgba(255, 255, 255, 0.7)'
  },
  inputContainer: {
    marginTop: 10
  },
  /*
  btnLogin: {
    width: WIDTH -55,
    height: 50,
    borderRadius: 25,
    backgroundColor: '#14873b',
    //opacity: 0.55,
    justifyContent: 'center',
    marginTop: 30
  },*/
  buttonLogin: {
    justifyContent: 'center',
    marginTop: 25,
  },
  text: {
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
    fontWeight: 'bold'
  },
  textEsqSenha: {
    color: 'black',
    fontSize: 16,
    marginTop: 15
  }
 
});