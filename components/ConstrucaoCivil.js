import React, {Component} from 'react';
import {StyleSheet, Text, View, TextInput, ScrollView, Picker, Alert} from 'react-native';
import { TextInputMask } from 'react-native-masked-text'
import {Button} from 'react-native-elements'
import {AutoGrowingTextInput} from 'react-native-autogrow-textinput'
import {openDatabase} from 'react-native-sqlite-storage'
import Icon from 'react-native-vector-icons/Octicons'

import {BD, TABLE} from './bd_config/banco'
const db = openDatabase({name: BD})

/*Usando uma biblioteca para os Botões:
https://github.com/APSL/react-native-button
*/
//import Button from 'apsl-react-native-button'

const valorNulo = 'R$0,00'

export default class ConstrucaoCivil extends Component{

  state = {
    cliente: '',
    servico: '',
    complemento: '',
    tipo: 'Construcao',
    valorReais: ''
  }

  //se retornar 0: o usuário não digitou nada (tá R$0,00)
  //se retornar 1: o usuário digitou mas depois apagou (ficou R$0,00)
  verificaValorNulo = (val) => {
    return valorNulo.localeCompare(val)
  }

  insert = () => {
    let that = this
    const {cliente, servico, complemento, tipo, valorReais} = this.state
    console.log('=> ', valorReais)
    if(cliente){
      if(servico){
        if(this.verificaValorNulo(valorReais) == -1){
          console.log('IF: ', valorReais)
          console.log('typeof: ', typeof valorReais)
          db.transaction(function(tx){
            tx.executeSql(
              'INSERT INTO ' + TABLE + ' (cliente, servico, complemento, valor, tipo) VALUES (?,?,?,?,?)',
              [cliente, servico, complemento, valorReais, tipo], (tx, results) => {
                console.log('Results: ', results.rowsAffected)
                if(results.rowsAffected > 0){
                  Alert.alert(
                    'ATENÇÃO:',
                    'Dados salvo com sucesso!', 
                    [{text: 'Ok!', 
                    onPress: () => that.props.navigation.navigate('TelaDetalhesOrcamento')}], {cancelable: false})

                }else{
                  alert('Falha no cadastro!')
                }
              }
            )
          })
          //zera os estados
          this.setState({
            cliente: '',
            servico: '',
            complemento: '',
            tipo: 'Construcao',
            valorReais: ''
          })
        }else{
          Alert.alert('ATENÇÃO:', 'Por favor preencha o valor do serviço.')
        }
      }else{
        Alert.alert('ATENÇÃO:','Por favor selecione o tipo de serviço.')
      }
    }else{
      Alert.alert('ATENÇÃO:','Por favor informe o nome do cliente.')
    }
  }

  /*DEBUG*/
  print = () => {
    console.log('state valorReais: ', this.state.valorReais)
    const n = valorNulo.localeCompare(this.state.valorReais)
    console.log('n: ', n)
  }

  render(){
    return(
      <View style={{flex: 1}}>
         {/*Titulo*/}
         <Text style={styles.title}>
          CONSTRUÇÃO CIVIL
        </Text>

        {/*Área de Scroll*/}
        <ScrollView keyboardDismissMode='on-drag'>
        
          {/*Nome do Cliente*/}
          <Text style={styles.label}>
            CLIENTE
          </Text>
          <TextInput 
            style={styles.input} 
            placeholder="Digite o nome do cliente" 
            onChangeText={input => this.setState({cliente: input})}
            value={this.state.cliente}
          />

          {/*Tipo de Serviço*/}
          <Text style={styles.label}>
            SERVIÇO
          </Text>
          <Picker
            style={styles.inputTextArea}
            selectedValue={this.state.servico}
            //style={{height:50, width: 200}}
            onValueChange={(itemValue, itemIndex) => {
              this.setState({servico: itemValue})
            }}
          > 
            <Picker.Item label="Selecione o tipo de serviço..." value='' />
            <Picker.Item label="Chapas" value="Chapas" style={{fontSize: 28}} />
            <Picker.Item label="Acessórios" value="Acessorios" style={{fontSize: 18}} />
            <Picker.Item label="Alvenaria" value="Alvenaria" style={{fontSize: 18}} />
          </Picker>

          {/*Text Area*/}
          <Text style={styles.label}>
            COMPLEMENTO
          </Text>

          <AutoGrowingTextInput style={{ marginLeft: 20, marginRight: 20}}>
            <TextInput
              style={styles.inputTextArea}
              underlineColorAndroid="transparent"
              placeholder="Digite informações adicionais aqui"
              placeholderTextColor="grey"
              multiline={true}
              returnKeyType='done'
              onChangeText={input => this.setState({complemento: input})}
              />
          </AutoGrowingTextInput>
        
          {/*Valor*/}
          <Text style={styles.label}>
            VALOR (R$)
          </Text>

          {/*Usando biblioteca externa para os valores. ATENÇÃO: o valor será uma String*/}
          <TextInputMask
            style={styles.input}
            type={'money'}
            options={{
              precision: 2,
              separator: ',',
              delimiter: '.',
              unit: 'R$',
              suffixUnit: ''
            }}
            value={this.state.valorReais}
            onChangeText={text => {
              this.setState({
                valorReais: text
              })
            }}
          />
          
          {/*Botões*/}
          <View style={{marginTop: 30}}>
            <View style={styles.container}>
              <Button 
                title='FINALIZAR'
                buttonStyle={styles.button} 
                titleStyle={{fontWeight:'bold', color: '#fff', fontSize:18}} 
                onPress={() => this.insert()}
                icon={
                  <Icon
                    name='tasklist'
                    size={18}
                    color='white'
                    style={{marginLeft: 25}}
                  />
                }
                iconRight
              />
            </View>

            <View style={styles.container}>
              <Button 
                title="VOLTAR"
                buttonStyle={styles.button} 
                titleStyle={{fontWeight:'bold', color: '#fff', fontSize:18}} 
                onPress={() => this.props.navigation.navigate('TelaDashboard')} 
                icon={
                  <Icon
                    name='reply'
                    size={25}
                    color='white'
                    style={{marginLeft: 20}}
                  />
                }
                iconRight
              />

            </View>
          </View>

          {/*DEBUG para mostrar o estado valorReais e o retorno da comparação dos valores.*/}
          {/*<Button onPress={() => this.print()}>
            Teste
          </Button>*/}

        </ScrollView>

      </View>

      
    );
  }
}

const styles = StyleSheet.create({
  title: {
    fontSize: 25, 
    fontWeight:'bold',      
    textAlign: 'center',
    textAlignVertical: 'center',
    backgroundColor: '#FF9932',
    color: '#fff',
    //paddingTop: 10,
    //flex: 1,
    //justifyContent: 'center',
    //alignItems: 'center',
    height: 50
  },
  label: {
    fontSize: 18,
    fontWeight: 'bold',
    marginTop: 30,
    marginLeft: 20

  },
  input: {
    marginTop: 2,
    marginLeft: 20,
    width: 360,
    borderBottomColor: 'gray', 
    borderBottomWidth: 1,
    fontSize: 18
  },
  inputTextArea: {
    borderColor:'grey', 
    borderWidth: 1, 
    marginTop: 10,
    marginLeft: 20,
    marginRight: 20,
    fontSize: 18
  },
  button: {
    backgroundColor:"#FF9932",
    marginTop: 20,
    width: 250,
  },
  container: {
    flexDirection: "row", 
    justifyContent: "center"
  }
})

