import React, {Component} from 'react';
import {StyleSheet, Text, View, ScrollView} from 'react-native';

export default class DetalheOrcamento extends Component{

  render(){
    return(
      <View style={{flex: 1}}>
        <Text style={styles.title}>
          DETALHES DO ORÇAMENTO
        </Text>
      
        <ScrollView>

          <Text>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur tristique elit id iaculis blandit. Sed ut dolor at nisi elementum posuere. Mauris bibendum velit id turpis tristique tincidunt. Nullam fermentum enim ac sem feugiat, at bibendum dolor tristique. Nam vel sodales leo. In maximus, libero sit amet accumsan aliquet, sapien arcu laoreet metus, nec imperdiet dolor lorem vel nulla. Morbi lacinia diam sem, eget molestie nisl euismod ac. Cras erat dolor, hendrerit in ultricies vestibulum, tincidunt eu tellus. Aliquam porta porta magna. Proin nec lacinia justo. Quisque sapien ex, consequat nec efficitur eget, tincidunt posuere nisi. Proin lacinia, mi sed venenatis tempus, velit enim sollicitudin nunc, ac consectetur purus ex vitae turpis. Pellentesque eget pretium turpis.
          </Text>

        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    fontSize: 25, 
    fontWeight:'bold',      
    textAlign: 'center',
    textAlignVertical: 'center',
    backgroundColor: '#FF9932',
    color: '#fff',
    //paddingTop: 10,
    //flex: 1,
    //justifyContent: 'center',
    //alignItems: 'center',
    height: 50
  },
})